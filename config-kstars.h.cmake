/* Define if you have fitsio.h */
#cmakedefine   HAVE_CFITSIO 1

/* Define if you have indidevapi.h */
#cmakedefine   HAVE_INDI 1

/* Define if you have xplanet */
#cmakedefine HAVE_XPLANET 1

/* Define if you have wcslibt */
#cmakedefine HAVE_WCSLIB 1

/* Define if you have astrometry */
#cmakedefine HAVE_ASTROMETRYNET 1
